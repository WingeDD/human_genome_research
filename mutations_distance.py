#!/usr/bin/python3
from typing import Tuple
import os
from Levenshtein import distance
from Bio import SeqIO
from utils import (
    get_config_file,
    get_json_data,
)


def read_sequence_record(fpath: str) -> list[str]:
    FORMAT = "fasta"
    records = SeqIO.parse(fpath, FORMAT) # should be only one
    for record in records:
        return record


def calc_distance(seq1, seq2) -> Tuple[float, float, float]:
    dist = distance(seq1, seq2)
    average_length = (len(seq1)+len(seq2))/2
    return (dist, average_length, dist/average_length)


def build_dist_matrix(sequences_dir_path: str, dist_func: callable) -> list[list]:
    files_paths: list[str] = [ os.path.join(sequences_dir_path, fname) for fname in os.listdir(sequences_dir_path) if os.path.isfile(os.path.join(sequences_dir_path, fname)) ]
    sequences_count: int = len(files_paths)
    dist_matrix: list[list] = [sequences_count*[None] for _ in range(sequences_count)]
    info: list = sequences_count * [None]
    for idx1 in range(sequences_count-1):
        rec1 = read_sequence_record(files_paths[idx1])
        seq1 = rec1.seq
        info[idx1] = (os.path.basename(files_paths[idx1]), rec1.id)
        for idx2 in range(idx1+1, sequences_count):
            rec2 = read_sequence_record(files_paths[idx2])
            seq2 = rec2.seq
            dist_matrix[idx1][idx2] = dist_func(seq1, seq2)
            dist_matrix[idx2][idx1] = dist_matrix[idx1][idx2]
            if (idx2 == (sequences_count-1)):
                if info[idx2] is None:
                    info[idx2] = (os.path.basename(files_paths[idx2]), rec2.id)
    return dist_matrix, info


if __name__ == "__main__":
    __default_config_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), r"conf_md.json")
    config: dict = get_json_data(get_config_file(__default_config_file))
    dist_matrix, info = build_dist_matrix(config["sequences_dir"], calc_distance)
    print(info)
    for l in dist_matrix:
        print(l)
