import os
import sys
import json
import yaml
import time
import re

INIT_MATRICES_CONFIG_FILE = "mahds_init_matrices.yaml"
METHOD_CONFIG_FILE = "mahds_method.yaml"
OUTPUT_CONFIG_FILE = "mahds_output.yaml"
SYM_MAPPING_CONFIG_FILE = "mahds_sym_mapping.yaml"
SYSTEM_CONFIG_FILE = "mahds_system.yaml"

UTIL_SRUN = "srun"
UTIL_MPIRUN = "mpirun"


re_significance_resp = re.compile(r"^.*?\s*?F\s*=\s*([+-]?(?:[0-9]+(?:[.][0-9]*)?|[.][0-9]+))\s*;?\s*mean\s*=\s*([+-]?(?:[0-9]+(?:[.][0-9]*)?|[.][0-9]+))\s*;?\s*variance\s*=\s*([+-]?(?:[0-9]+(?:[.][0-9]*)?|[.][0-9]+))\s*;?\s*Z\s*=\s*([+-]?(?:[0-9]+(?:[.][0-9]*)?|[.][0-9]+));?\s*$")
re_bs_resp = re.compile(r"CS\s*score\s*=\s*(\S+)$")
re_bsb_resp = re.compile(r"\s+(.+?)[\n\r]")
re_bsr_resp = re.compile(r"CS\s*score\s*=\s*(\S+)$")

re_queue = re.compile(r'^\s*Queue:\s*\w+\s*Running:\s*(\d+)\s*;\s*Queued:\s*(\d+)\s*;\s*Pre-runned:\s*(\d+)')
re_correct_cleo_resp = re.compile(r'\s*Using\s*queue\s*\w+\s*Successfully\s*added\s*to\s*queue\s*\w+\s*\(ID=\d+\)\s*')
re_correct_slurm_resp = re.compile(r'^\s*Submitted\s*batch\s*job\s*\d+\s*$')


def get_config_file(default:str=None) -> str:
    if len(sys.argv) > 2:
        raise Exception(f'expected 0 or 1 command line argument, got: {len(sys.argv)-1}.')
    elif len(sys.argv) == 2:
        return sys.argv[1]
    else:
        return default


def get_json_data(fname:str) -> dict:
    with open(fname, "r") as file:
        return json.load(file)

